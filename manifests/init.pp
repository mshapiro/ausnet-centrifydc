# Class: centrifydc
#
#  Manages the centrifydc package and service
#
# Parameters:
#
#  [enabled]
#   Desc: Determine whether the centrifydc service should be installed and running
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'centrifydc': enabled => true, }
#
class centrifydc(
  $enabled = false,
  $package_name = 'CentrifyDC',
  $service_name = 'centrifydc',
  $centrifydc_domain,
  $centrifydc_ou,
  $centrifydc_zone)
{
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    $file_ensure = 'file'
    $directory_ensure = 'directory'
    Package["$package_name"] ~> Exec['centrifydc_copy_krb5']
    Exec['centrifydc_copy_krb5'] ~> Exec['centrifydc_authenticate']
    Exec['centrifydc_authenticate'] ~> Exec['centrifydc_domain_join']
    Exec['centrifydc_domain_join'] -> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    $file_ensure = 'absent'
    $directory_ensure = 'absent'
    Service["$service_name"] -> Package["$package_name"]
  }
  # resource defaults
  File {
    owner    => 'root',
    group    => 'root',
    mode     => '0600',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => 'system_u',
  }
  Exec {
    path => '/usr/bin:/bin:/usr/sbin:/sbin',
  }
  # resources
  file { '/root/centrifydc':
    ensure   => "$directory_ensure",
    mode     => '0700',
  }
  file { '/root/centrifydc/svc.keytab':
    ensure   => "$file_ensure",
    source   => 'puppet:///modules/centrifydc/svc.keytab',
  }
  file { '/root/centrifydc/krb5.conf':
    ensure   =>  "$file_ensure",
    source   => 'puppet:///modules/centrifydc/krb5.conf',
  }
  package { "$package_name":
    ensure   =>  $package_ensure,
  }
  service { "$service_name":
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
  exec { 'centrifydc_copy_krb5':
    command     =>  "cp -f /root/centrifydc/krb5.conf /etc/",
    unless      =>  "adinfo | grep -q 'CentrifyDC\smode.*connected'",
  }
  exec { 'centrifydc_authenticate':
    command     =>  "/usr/share/centrifydc/kerberos/bin/kinit -kt /root/centrifydc/svc.keytab joiner",
    refreshonly =>  true,
  }
  exec { 'centrifydc_domain_join':
    command     =>  "adjoin -c $centrifydc_ou $centrifydc_domain -z $centrifydc_zone",
    refreshonly =>  true,
  }
}

